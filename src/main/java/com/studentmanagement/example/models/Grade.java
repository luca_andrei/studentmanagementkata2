package com.studentmanagement.example.models;

import com.studentmanagement.example.DTO.GradeDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "grade")
public class Grade implements Serializable {

    private Grade() {};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private Integer rate;

    @Column(nullable = true)
    private String description;

    @ManyToOne
    @JoinColumn(name = "student")
    private Student student;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public interface IRate {
        IDescription rate(Integer rate);
    }

    public interface IDescription {
        IBuild description(String description);
    }

    public interface IBuild {
        Grade build();
    }

    public static class Builder implements IRate, IDescription, IBuild {
        private Grade grade = new Grade();


        @Override
        public IDescription rate(Integer rate) {
            grade.rate = rate;
            return this;
        }

        @Override
        public IBuild description(String description) {
            grade.description = description;
            return this;
        }

        @Override
        public Grade build() {
            return grade;
        }
    }
}
