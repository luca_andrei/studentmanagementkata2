package com.studentmanagement.example.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "student")
public class Student implements Serializable{

    private Student() {};
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(length = 50, nullable = false)
    private String firstName;

    @Column(length = 50, nullable = false)
    private String lastName;

    /**
     * Eager inseamna ca in momentul in care este creat studentul este adusa in memorie
     * si aceasta lista de note pe cand specificand Lazy, lista de note va fi adusa in
     * memorie doar in momentul in care apelam metoda getGrades();
     *
     * O abordare lazy se bazeaza pe principiul "aman cat de mult pot momentul" si in general,
     * este o abordare foarte ok.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student", cascade={CascadeType.ALL})
    private List<Grade> grades;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    public interface IFirstName {
        ILastName firstName(String firstName);
    }

    public interface ILastName {
        IBuild lastName(String lastName);
    }

    public interface IBuild {
        Student build();
    }

    public static class Builder implements IFirstName, ILastName, IBuild {
        private Student student;

        @Override
        public ILastName firstName(String firstName) {
            student.firstName = firstName;
            return this;
        }

        @Override
        public IBuild lastName(String lastName) {
            student.lastName = lastName;
            return this;
        }

        @Override
        public Student build() {
            return student;
        }
    }
}
