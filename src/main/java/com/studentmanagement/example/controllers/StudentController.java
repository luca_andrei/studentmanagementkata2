package com.studentmanagement.example.controllers;

import com.google.common.collect.Lists;
import com.studentmanagement.example.DTO.CreatingStudentDto;
import com.studentmanagement.example.DTO.StudentDto;
import com.studentmanagement.example.models.Student;
import com.studentmanagement.example.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/students")
public class StudentController {
    @Autowired
    private StudentService service;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<StudentDto>> get() {
        List<Student> students = this.service.getAll();
        if (students.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        List<StudentDto> result = Lists.transform(students, student -> toDto(student));
        if (students.size() != result.size()) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<StudentDto> addStudent(@RequestBody CreatingStudentDto studentDto) {
        Student student = toCreatingModel(studentDto);
        Student savedStudent = this.service.save(student);
        return new ResponseEntity<>(toDto(savedStudent), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<StudentDto> getStudentById(@PathVariable("id") Long id) {
        Student student = this.service.getById(id);
        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(toDto(student), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<StudentDto> updateStudentById(@PathVariable("id") Long id ,@RequestBody CreatingStudentDto studentDto) {
        Student student = this.service.getById(id);

        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Student newStudent = toCreatingModel(studentDto);
        student.setFirstName(newStudent.getFirstName());
        student.setLastName(newStudent.getLastName());

        this.service.save(student);

        return new ResponseEntity<>(toDto(newStudent), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<StudentDto> deleteStudentById(@PathVariable("id") Long id) {
        Student student = this.service.getById(id);
        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        this.service.delete(id);
        /* Ar fi trebuit 204 dar nu l-am gasit printre macro-uri. */
        return new ResponseEntity<>(toDto(student), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<List<StudentDto>> delete() {
        List<Student> studentList = service.getAll();
        for (Student student : studentList) {
            this.service.delete(student.getId());
        }

        return new ResponseEntity<>(Lists.transform(studentList, student -> toDto(student)), HttpStatus.OK);
    }



    private StudentDto toDto(Student student) {
        return new StudentDto.Builder().firstName(student.getFirstName()).lastName(student.getLastName()).id(student.getId()).build();
    }

    private Student toCreatingModel(CreatingStudentDto dto) {
        return new Student.Builder().firstName(dto.firstName).lastName(dto.lastName).build();
    }
}
