package com.studentmanagement.example.DTO;

public class StudentDto extends CreatingStudentDto {
    public Long id;

    private StudentDto() {};

    public interface IFirstName {
        ILastName firstName(String firstName);
    }

    public interface ILastName {
        IId lastName(String lastName);
    }

    public interface IId {
        IBuild  id(Long id);
    }

    public interface IBuild {
        StudentDto build();
    }

    public static class Builder implements IFirstName, ILastName, StudentDto.IId, StudentDto.IBuild {

        private StudentDto studentDto = new StudentDto();

        @Override
        public ILastName firstName(String firstName) {
            studentDto.firstName = firstName;
            return this;
        }

        @Override
        public IId lastName(String lastName) {
            studentDto.lastName = lastName;
            return this;
        }

        @Override
        public IBuild id(Long id) {
            studentDto.id = id;
            return this;
        }

        @Override
        public StudentDto build() {
            return studentDto;
        }
    }
}
