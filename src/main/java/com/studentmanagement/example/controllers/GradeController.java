package com.studentmanagement.example.controllers;

import com.google.common.collect.Lists;
import com.studentmanagement.example.DTO.CreatingGradeDto;
import com.studentmanagement.example.DTO.GradeDto;
import com.studentmanagement.example.models.Grade;
import com.studentmanagement.example.models.Student;
import com.studentmanagement.example.services.GradeService;
import com.studentmanagement.example.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@RestController
@RequestMapping("v1/students")
public class GradeController {
    @Autowired
    private GradeService gradeService;

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/{studentId}/grades", method = RequestMethod.GET)
    public ResponseEntity<List<GradeDto>> getGradesByStudentId(@PathVariable("studentId") Long studentId) {
        Student student = this.studentService.getById(studentId);
        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        //??
        student.getGrades().size();
        List<Grade> grades = student.getGrades();
        //??
        // voiati sa verific student.getGrades().size() == grades.size()?
        // e acelasi obiect
        grades.size();

        List<GradeDto> dtoGrades = Lists.transform(grades, grade -> toDto(grade));

        if (dtoGrades.size() != grades.size()) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(dtoGrades ,HttpStatus.OK);
    }

    @RequestMapping(value = "/{studentId}/grades", method = RequestMethod.POST)
    public ResponseEntity<GradeDto> addGrade(@PathVariable("studentId") Long studentId, @RequestBody CreatingGradeDto dto) {
        Student student = this.studentService.getById(studentId);
        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Grade newGrade = toCreatingModel(dto);

        if (newGrade == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        newGrade.setStudent(student);

        Grade savedGrade = this.gradeService.save(newGrade);

        return new ResponseEntity<>(toDto(savedGrade), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{studentId}/grades/{gradeId}", method = RequestMethod.PUT)
    public ResponseEntity<GradeDto> updateGrade(@PathVariable Long studentId, @PathVariable Long gradeId, @RequestBody CreatingGradeDto dto) {
        Student student = studentService.getById(studentId);
        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<Grade> grades = student.getGrades();

        if (grades == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Grade grade = null;

        for (Grade parsingGrade : grades) {
            if (parsingGrade.getId() == gradeId) {
                grade = parsingGrade;
                break;
            }
        }
        if (grade == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        /* Nu as vrea sa las sa imi modifice studentul. */
        Grade newGrade = toCreatingModel(dto);
        grade.setDescription(newGrade.getDescription());
        grade.setRate(newGrade.getRate());
        this.gradeService.save(grade);

        return new ResponseEntity<>(toDto(grade), HttpStatus.OK);
    }

    @RequestMapping(value = "/{studentId}/grades", method = RequestMethod.DELETE)
    public ResponseEntity<List<GradeDto>> delete(@PathVariable("studentId") Long studentId) {
        List<Grade> grades = this.gradeService.getGradesByStudentId(studentId);
        if (grades == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        for (Grade grade : grades) {
            this.gradeService.delete(grade.getId());
        }
        return new ResponseEntity<>(Lists.transform(grades, grade -> toDto(grade)), HttpStatus.OK);
    }

    @RequestMapping(value = "/{studentId}/grades/{gradeId}", method = RequestMethod.DELETE)
    public ResponseEntity<GradeDto> delete(@PathVariable("studentId") Long studentId, @PathVariable("gradeId") Long gradeId) {
        List<Grade> grades = this.gradeService.getGradesByStudentId(studentId);
        if (grades == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Grade grade = null;
        for (Grade parsingGrade : grades) {
            if (parsingGrade.getId() == gradeId) {
                grade = parsingGrade;
                break;
            }
        }
        if (grade == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        this.gradeService.delete(gradeId);
        return new ResponseEntity<>(toDto(grade) , HttpStatus.OK);
    }

    private GradeDto toDto(Grade grade) {
        return new GradeDto.Builder().rate(grade.getRate()).description(grade.getDescription()).id(grade.getId()).build();
    }

    private Grade toCreatingModel(CreatingGradeDto dto) {
        return new Grade.Builder().rate(dto.rate).description(dto.description).build();
    }
}

