package com.studentmanagement.example.DTO;

public class GradeDto extends CreatingGradeDto {
    public Long id;

    private GradeDto() {};

    public interface IRate {
        IDescription rate(Integer rate);
    }

    public interface IDescription {
        IId description(String description);
    }

    public interface IId {
        IBuild id(Long id);
    }

    public interface IBuild {
        GradeDto build();
    }

    public static class Builder implements IRate, IDescription, IBuild, IId {
        private GradeDto gradeDto = new GradeDto();

        public Builder() {
        }

        @Override
        public IDescription rate(Integer rate) {
            gradeDto.rate = rate;
            return this;
        }

        @Override
        public IId description(String description) {
            gradeDto.description = description;
            return this;
        }

        @Override
        public IBuild id(Long id) {
            gradeDto.id = id;
            return this;
        }

        @Override
        public GradeDto build() {
            return gradeDto;
        }
            }
}


